# MySQL Exercise

Docker를 사용해 MySQL과 Adminer를 사용해보며 기본적인 사용법을 익혀봅시다.

## Installation

### 1. Docker 설치

Docker를 설치합니다.
Docker Hub에 가입해야 합니다.

* Windows: https://docs.docker.com/docker-for-windows/install/
* macOS: https://docs.docker.com/docker-for-mac/install/

### 2. MySQL 및 Adminer 컨테이너 실행

#### 1. 이미지 Pull

```bash
$ docker pull mysql:latest
$ docker pull adminer:latest
```

##### 확인

```bash
$ docker image ls
```

`mysql`과 `adminer` 이미지가 존재해야 합니다.

#### 2. 컨테이너 실행

```bash
$ docker run --name {name of the container} -e MYSQL_ROOT_PASSWORD={password of root user} -d mysql:latest
$ docker run --name {name of the container} --link {name of the *database* container}:db -p 8080:8080 -d adminer:latest
```

##### 확인

```bash
$ docker ps
```

`mysql`과 `adminer` 이미지의 컨테이너가 구동중이어야 합니다.

#### 3. Adminer 접속

잘 따라했다면 [http://localhost:8080](http://localhost:8080)로 접속했을 때 Adminer 페이지를 확인할 수 있습니다. MySQL 컨테이너 실행 시 입력한 정보로 로그인하세요. 데이터베이스는 생성하지 않았으므로 비워둡니다.

## Instructions

### 1. Table and Schema Basics

임의의 이름으로 데이터베이스를 생성하세요.

### 2. SQL - Basic CRUD

임의의 이름으로 데이터베이스를 생성하고, 주어진 `crud.sql.gz` 파일을 import 하세요.

#### 1. `INSERT INTO`

다음과 같은 entity를 테이블에 추가하세요.

|**Name**|**Salary**|**Role**|
|-|-|-|
|Alice|5367|Developer|

#### 2. `SELECT`

##### 1.

전체 직원 정보를 알고싶습니다.
모든 entity의 모든 column을 읽어보세요.

> SELECT * FROM employee

##### 2.

Bob의 정보를 알고싶습니다.
Name이 'Bob'인 entity의 모든 column을 읽어보세요.

> SELECT * FROM employee WHERE name='Bob'

##### 3.

Carol의 salary를 알고싶습니다.
Name이 'Carol'인 entity의 salary column만 읽어보세요.

> SELECT salary FROM employee WHERE name='Carol'

##### 4.

Salary가 5000 이상인 모든 직원의 정보를 알고싶습니다.
Salary가 5000 이상인 entity의 모든 column을 읽어보세요.

> SELECT * FROM employee WHERE salary > 5000

##### 5.

Salary가 7000 이상인 직원의 name과 role을 알고싶습니다.
Salary가 7000 이상인 entity의 name과 role column을 읽어보세요.

> SELECT name, role FROM employee WHERE salary > 7000

#### 3. `UPDATE`

##### 1.

Dave는 manager가 되었습니다.
Name이 'Dave'인 entity의 role을 'Manager'로 수정하세요.

> UPDATE employee SET role = 'Manager' WHERE name = 'Dave'

##### 2.

앞으로는 hacker를 security engineer라고 부르기로 결정했습니다.
Role이 'Hacker'인 모든 entity의 role을 'Security Engineer'로 수정하세요.

> UPDATE employee SET role = 'Security Engineer' WHERE role = 'Hacker'

#### 4. `ORDER BY`

##### 1.

모든 entity를 salary를 기준으로 내림차순 정렬하여 읽어보세요.

> SELECT * FROM employee ORDER BY salary DESC

##### 2.

Role이 'Developer'인 모든 entity를 salary를 기준으로 오름차순 정렬하여 읽어보세요.

> SELECT * FROM employee WHERE role = 'Developer' ORDER BY salary

#### 5. `LIMIT`

##### 1.

모든 entity를 salary를 기준으로 내림차순 정렬하여 상위 3위까지만 읽어보세요.

> SELECT * FROM employee ORDER BY salary DESC LIMIT 3

##### 2.

Role이 'Developer'인 모든 entity를 salary를 기준으로 오름차순 정렬하여 하위 3위까지만 읽어보세요.

> SELECT * FROM employee WHERE role = 'Developer' ORDER BY salary LIMIT 3

#### 6. `OFFSET`

##### 1.

모든 entity를 salary를 기준으로 내림차순 정렬하여 상위 4위부터 6위까지만 읽어보세요.

> SELECT * FROM employee ORDER BY salary DESC limit 3 offset 3

##### 2.

Role이 'Security Engineer'인 모든 entity를 salary를 기준으로 오름차순 정렬하여 하위 4위부터 6위까지만 읽어보세요.

> SELECT * FROM employee WHERE role = 'Developer' ORDER BY salary LIMIT 3 offset 3

#### 7. `DISTINCT`

##### 1.

저장된 entity들이 가지고 있는 role을 읽어보세요.
같은 role은 하나씩만 출력되어야 합니다.

> SELECT DISTINCT role FROM employee

#### 8. `GROUP BY`

##### 1.

각 role별 직원 수가 알고싶습니다.
각각의 role별로 직원이 몇 명 있는지 출력해보세요.

> SELECT role, COUNT(role) FROM employee GROUP BY role

##### 2.

각 role별 평균 salary가 알고싶습니다.
각각의 role별로 salry의 평균이 얼마인지 출력해보세요.

> SELECT role, AVG(salary) FROM employee GROUP BY role

#### 9. `DELETE`

##### 1.

Eve는 퇴사했습니다.
Name이 'Eve'인 entity를 삭제하세요.

> DELETE FROM employee WHERE name = 'Eve'

##### 2.

Salary가 3000 이하인 직원이 모두 퇴사했습니다.
Salary가 3000 이하인 entity를 모두 삭제하세요.

> DELETE FROM employee WHERE salary < 3000

### 3. Relation and Join Query

임의의 이름으로 데이터베이스를 생성하세요.

#### 1. Create Tables

##### 1. Employee Table

|**Column Name**|**Data Type**|**Others**|
|-|-|-|
|id|integer|auto increment, primary key|
|name|varchar(16)|-|
|team|integer|foreign key(team)|

##### 2. Team Table

|**Column Name**|**Data Type**|**Others**|
|-|-|-|
|id|integer|auto increment, primary key|
|name|varchar(16)|unique key|

#### 2. Create Relations

Employee 테이블의 team column은 team 테이블의 id column 값을 저장하며 employee가 소속된 팀을 의미합니다.
한 employee는 하나의 team에만 소속될 수 있으며, 한 team에는 여러 employee가 소속될 수 있습니다(1:n relation).

#### 3. Insert Data

##### 1. Team

3개의 team이 존재합니다.
다음과 같은 entity를 team 테이블에 추가하세요.

|**Name**|
|-|
|Development|
|Security|
|Management|

##### 2. Employee

6명의 employee가 존재합니다.
다음과 같은 entity를 테이블에 추가하세요.

|**Name**|**Team**|
|-|-|
|Alice|Development|
|Bob|Security|
|Carol|Management|
|Dave|Development|
|Eve|Security|
|Frank|Management|

###### 추가 사항

존재하지 않는 team에 소속된 employee를 등록하려고 시도하면 어떻게 되는지 확인해봅시다.
다음과 같은 entity를 테이블에 추가해보세요.

|**Name**|**Team**|
|-|-|
|Grace|999|

#### 4. Join Query

##### 1. Employee 정보 읽기

모든 employee의 정보를 알고싶습니다.
모든 employee의 모든 column을 읽어보세요.

##### 2. Team 정보 읽기

모든 team의 정보를 알고싶습니다.
모든 team의 모든 column을 읽어보세요.

##### 3. Join

모든 employee의 정보를 알고싶습니다.
다만 이번에는 team의 id 대신 name을 가져오고 싶습니다.
모든 employee에 대해 employee의 name과, 각 employee가 소속된 team의 name을 함께 읽어보세요.

### 4. Many to Many Relation

임의의 이름으로 데이터베이스를 생성하고, 주어진 `m2m.sql.gz` 파일을 import 하세요.

#### 1. Create Intermediate Table

##### 1. Create Intermediate Table

한 명의 student는 여러 club에 소속될 수 있으며, 한 club에는 여러 명의 student가 소속되어 있을 수 있습니다(m:n relation).

##### 2. Create Relations

Intermediate table에 적절한 relation을 설정해 줍니다.

##### 3. Insert Data

6명의 student는 각각 다음과 같은 club에 가입했습니다.
이를 표현하기 위한 entity를 intermediate table에 추가하세요.

|**Name**|**Club**|
|-|-|
|Alice|Hacking, Programming|
|Bob|Programming, Soccer|
|Carol|Soccer, Hacking|
|Dave|Hacking, Programming|
|Eve|Programming, Soccer|
|Frank|Soccer, Hacking|

#### 2. Join Query

##### 1.

Alice가 소속되어 있는 club을 알고싶습니다.
Alice가 소속되어 있는 club의 목록을 읽어보세요.

##### 2.

Hacking club에 소속되어 있는 student를 알고싶습니다.
Hacking club에 소속되어 있는 student의 목록을 읽어보세요.
